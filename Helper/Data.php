<?php
/**
 * Trilogy Countrystore
 *
 * @category  Trilogy
 * @package   Trilogy_Countrystore 
 * @copyright 2015 Ekvitech
 * Developed by Arvind Rawat 
 */

namespace Trilogy\Countrystore\Helper;
 
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\ScopeInterface;
 
class Data extends AbstractHelper
{
    
    protected $_scopeConfig;
 
   
    public function __construct(
       Context $context,
       ScopeConfigInterface $scopeConfig
    ) {
       parent::__construct($context);
       $this->_scopeConfig = $scopeConfig;
    }
    
    public function getCountryCode($country)
    {
        return $this->scopeConfig->getValue('countrycode/'.$country.'/code', ScopeInterface::SCOPE_STORE
        );
    }
  
}