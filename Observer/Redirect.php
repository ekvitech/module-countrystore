<?php
/**
 * Trilogy Countrystore
 *
 * @category  Trilogy
 * @package   Trilogy_Countrystore 
 * @copyright 2015 Ekvitech
 * Developed by Arvind Rawat 
 */


namespace Trilogy\Countrystore\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Store\Model\ScopeInterface;

class Redirect implements ObserverInterface
{

    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $urlBuilder;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;
    protected $scopeConfig;

    public function __construct(
        \Magento\Framework\UrlInterface $urlBuilder,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        \Magento\Framework\App\RequestInterface $request,
        \Magento\Framework\App\Cache\TypeListInterface $cacheTypeList,
        \Magento\Framework\App\Cache\Frontend\Pool $cacheFrontendPool,
        \Trilogy\Countrystore\Helper\Data $moduleHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    )
    {
        $this->urlBuilder = $urlBuilder;
        $this->_storeManager = $storeManager;
        $this->_moduleHelper = $moduleHelper;
        $this->cookieManager = $cookieManager;
        $this->_request = $request;
        $this->_cacheTypeList = $cacheTypeList;
        $this->_cacheFrontendPool = $cacheFrontendPool;
        $this->_scopeConfig = $scopeConfig;
    }

    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $controller = $observer->getControllerAction();
        $cookieVal=@$this->cookieManager->getCookie('TrilogyCountrySelectorValue');
        $currentUrl = $this->urlBuilder->getCurrentUrl();
        $cookieValMod=strtolower(preg_replace('/\s+/', '', $cookieVal));
        $code=$this->_moduleHelper->getCountryCode($cookieValMod);
        // not required as per new specifications
        /*if($cookieVal && @$this->_request->getParam('countrypref')=='')
        {
            if (preg_match('/'.$code.'/',$currentUrl)){
                
            }
            else{
                $types = array('collections');
                foreach ($types as $type) {
                    $this->_cacheTypeList->cleanType($type);
                }
                foreach ($this->_cacheFrontendPool as $cacheFrontend) {
                    $cacheFrontend->getBackend()->clean();
                }

                $urlArr=explode($this->_scopeConfig->getValue('web/unsecure/base_url',\Magento\Store\Model\ScopeInterface::SCOPE_STORE),$currentUrl);
                
                
                if(@$urlArr[1])
                   $redirectTo=$this->_storeManager->getWebsite($code)->getDefaultStore()->getBaseUrl().$urlArr[1];
                else
                    $redirectTo=$this->_storeManager->getWebsite($code)->getDefaultStore()->getBaseUrl();

                
                $controller->getResponse()->setRedirect($redirectTo);
            }
        }*/
    }
}
