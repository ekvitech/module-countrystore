<?php
/**
 * Trilogy Countrystore
 *
 * @category  Trilogy
 * @package   Trilogy_Countrystore 
 * @copyright 2015 Ekvitech
 * Developed by Arvind Rawat 
 */
namespace Trilogy\Countrystore\Controller\Index;
 
use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;
use Magento\Store\Model\StoreManagerInterface as StoreManagerInterface;
use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\Stdlib\Cookie\PublicCookieMetadata;
use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    const COOKIE_NAME = 'TrilogyCountrySelectorValue';
    const COOKIE_PERIOD = 315360000;

    protected $cookieManager;
    protected $cookieMetadataFactory;
    protected $sessionManager;
    protected $publicCookieMetaData;
    protected $response;
    protected $_eventManager = null;
    public function __construct(
        Context $context,
        PageFactory $resultPageFactory,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        SessionManagerInterface $sessionManager,
        PublicCookieMetadata $publicCookieMetaData,
        StoreManagerInterface $StoreManagerInterface,
        \Trilogy\Countrystore\Helper\Data $moduleHelper
    )
    {
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
        $this->_moduleHelper = $moduleHelper;
        $this->cookieManager = $cookieManager;
        $this->cookieMetadataFactory = $cookieMetadataFactory;
        $this->sessionManager = $sessionManager;
        $this->publicCookieMetaData = $publicCookieMetaData;
        $this->_storeManager = $StoreManagerInterface;
    }
 
    public function execute()
    {//var_dump($this->getRequest()->isPost());exit;
        $getCookieValue=$this->cookieManager->getCookie(self::COOKIE_NAME);

        if($this->getRequest()->isPost()){
            if(@$this->getRequest()->getPost('countrypref'))// country selection
            {
                $prevcode='';
                // get existing value of cookie
                if(@$this->cookieManager->getCookie(self::COOKIE_NAME)){
                    $prevValue=@$this->cookieManager->getCookie(self::COOKIE_NAME);
                    $prevstr=strtolower(preg_replace('/\s+/', '', $prevValue));
                    $prevcode=$this->_moduleHelper->getCountryCode($prevstr);
                }
                
                $value=$this->getRequest()->getPost('countrypref');
                $str=strtolower(preg_replace('/\s+/', '', $value));
                $code=$this->_moduleHelper->getCountryCode($str);
                if($code=='')
                    $code='1';// default store
                if ($value) { 
                    //set cookie
                    $cookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
                            ->setDuration(self::COOKIE_PERIOD)
                            ->setPath('/')
                            ->setHttpOnly(false);
                        
                    $this->cookieManager->setPublicCookie(self::COOKIE_NAME, $value, $cookieMetadata);
                    
                    if($prevcode=='') {// if no previous cookie value then send to selected store
                        $redirectTo=$this->_storeManager->getWebsite($code)->getDefaultStore()->getBaseUrl();
                        $this->getResponse()->setRedirect($redirectTo);
                    }
                    else {
                        $urlArr=explode($this->_storeManager->getWebsite($prevcode)->getDefaultStore()->getBaseUrl(),$this->_redirect->getRedirectUrl());
                        if(@$urlArr[1])
                            $redirectTo=$this->_storeManager->getWebsite($code)->getDefaultStore()->getBaseUrl().$urlArr[1];
                        else
                            $redirectTo=$this->_storeManager->getWebsite($code)->getDefaultStore()->getBaseUrl();
                        
                        $this->getResponse()->setRedirect($redirectTo);
                    }
                }
                else{
                    $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
                }
            }
            else if(@$this->getRequest()->getPost('delcook')==1){ // delete selection
                    if($getCookieValue){
                        $prevcode='';
                        if(@$getCookieValue){
                            
                            $prevstr=strtolower(preg_replace('/\s+/', '', $getCookieValue));
                            $prevcode=$this->_moduleHelper->getCountryCode($prevstr);
                        }
                        $urlArr=explode($this->_storeManager->getWebsite($prevcode)->getDefaultStore()->getBaseUrl(),$this->_redirect->getRedirectUrl());
                        if(@$urlArr[1])
                            $redirectTo=$this->_storeManager->getWebsite('1')->getDefaultStore()->getBaseUrl().$urlArr[1];
                        else
                            $redirectTo=$this->_storeManager->getWebsite('1')->getDefaultStore()->getBaseUrl();
                        
                        $cookieMetadata = $this->cookieMetadataFactory->createPublicCookieMetadata()
                            ->setDuration(self::COOKIE_PERIOD)
                            ->setPath('/')
                            ->setHttpOnly(false);
                        $this->cookieManager->deleteCookie(self::COOKIE_NAME,$cookieMetadata);

                        $this->getResponse()->setRedirect($redirectTo);
                        }
                        else{
                            $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
                        }
                    
            }
        } 
        else
        {
            $this->getResponse()->setRedirect($this->_redirect->getRedirectUrl());
        }
    }
}