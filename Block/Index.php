<?php
/**
 * Trilogy Countrystore
 *
 * @category  Trilogy
 * @package   Trilogy_Countrystore 
 * @copyright 2015 Ekvitech
 * Developed by Arvind Rawat 
 */
namespace Trilogy\Countrystore\Block;
use Magento\Store\Model\StoreManagerInterface as StoreManagerInterface;
class Index extends \Magento\Framework\View\Element\Template
{
	public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        StoreManagerInterface $StoreManagerInterface,
        \Trilogy\Countrystore\Helper\Data $moduleHelper,
        \Magento\Framework\Stdlib\CookieManagerInterface $cookieManager,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->_countryCollectionFactory = $countryCollectionFactory;
        $this->_storeManager = $StoreManagerInterface;
        $this->cookieManager = $cookieManager;
        $this->_moduleHelper = $moduleHelper;
    }

    protected function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    //Get country list
    public function getCountries() 
    {
        $countryCollection = $this->_countryCollectionFactory->create()->loadByStore();
        return $countryCollection->toOptionArray();
    }
    //Generates form action for template
    public function getFormActionUrl()
    {
        return $this->_storeManager->getWebsite($this->_storeManager->getStore()->getWebsiteId())->getDefaultStore()->getBaseUrl().'countryselector/index/index';
    }
    public function getMyCookie()
    {
        return $this->cookieManager->getCookie('TrilogyCountrySelectorValue');
    }
    //Get selected country code
    public function getCountry()
    {
        $storeCode=$this->_storeManager->getStore()->getCode();
        return $this->_moduleHelper->getCountryCode($storeCode);
    }
}